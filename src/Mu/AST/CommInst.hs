--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE DeriveGeneric #-}

module Mu.AST.CommInst (CommInst (..)) where

import Data.Binary (Binary)
import GHC.Generics (Generic)

data CommInst
    = CiUvmNewStack
    | CiUvmKillStack
    | CiUvmThreadExit
    | CiUvmCurrentStack
    | CiUvmSetThreadlocal
    | CiUvmGetThreadlocal
    | CiUvmTr64IsFp
    | CiUvmTr64IsInt
    | CiUvmTr64IsRef
    | CiUvmTr64FromFp
    | CiUvmTr64FromInt
    | CiUvmTr64FromRef
    | CiUvmTr64ToFp
    | CiUvmTr64ToInt
    | CiUvmTr64ToRef
    | CiUvmTr64ToTag
    | CiUvmFutexWait
    | CiUvmFutexWaitTimeout
    | CiUvmFutexWake
    | CiUvmFutexCmpRequeue
    | CiUvmKillDependency
    | CiUvmNativePin
    | CiUvmNativeUnpin
    | CiUvmNativeGetAddr
    | CiUvmNativeExpose
    | CiUvmNativeUnexpose
    | CiUvmNativeGetCookie
    | CiUvmMetaIdOf
    | CiUvmMetaNameOf
    | CiUvmMetaLoadBundle
    | CiUvmMetaLoadHail
    | CiUvmMetaNewCursor
    | CiUvmMetaNextFrame
    | CiUvmMetaCopyCursor
    | CiUvmMetaCloseCursor
    | CiUvmMetaCurFunc
    | CiUvmMetaCurFuncVer
    | CiUvmMetaCurInst
    | CiUvmMetaDumpKeepalives
    | CiUvmMetaPopFramesTo
    | CiUvmMetaPushFrame
    | CiUvmMetaEnableWatchpoint
    | CiUvmMetaDisableWatchpoint
    | CiUvmMetaSetTrapHandler
    | CiUvmMetaConstantById
    | CiUvmMetaGlobalById
    | CiUvmMetaFuncById
    | CiUvmMetaExpfuncById
    | CiUvmIrbuilderNewIrBuilder
    | CiUvmIrbuilderLoad
    | CiUvmIrbuilderAbort
    | CiUvmIrbuilderGenSym
    | CiUvmIrbuilderNewTypeInt
    | CiUvmIrbuilderNewTypeFloat
    | CiUvmIrbuilderNewTypeDouble
    | CiUvmIrbuilderNewTypeUptr
    | CiUvmIrbuilderNewTypeUfuncptr
    | CiUvmIrbuilderNewTypeStruct
    | CiUvmIrbuilderNewTypeHybrid
    | CiUvmIrbuilderNewTypeArray
    | CiUvmIrbuilderNewTypeVector
    | CiUvmIrbuilderNewTypeVoid
    | CiUvmIrbuilderNewTypeRef
    | CiUvmIrbuilderNewTypeIref
    | CiUvmIrbuilderNewTypeWeakref
    | CiUvmIrbuilderNewTypeFuncref
    | CiUvmIrbuilderNewTypeTagref64
    | CiUvmIrbuilderNewTypeThreadref
    | CiUvmIrbuilderNewTypeStackref
    | CiUvmIrbuilderNewTypeFramecursorref
    | CiUvmIrbuilderNewTypeIrbuilderref
    | CiUvmIrbuilderNewFuncsig
    | CiUvmIrbuilderNewConstInt
    | CiUvmIrbuilderNewConstIntEx
    | CiUvmIrbuilderNewConstFloat
    | CiUvmIrbuilderNewConstDouble
    | CiUvmIrbuilderNewConstNull
    | CiUvmIrbuilderNewConstSeq
    | CiUvmIrbuilderNewConstExtern
    | CiUvmIrbuilderNewGlobalCell
    | CiUvmIrbuilderNewFunc
    | CiUvmIrbuilderNewExpFunc
    | CiUvmIrbuilderNewFuncVer
    | CiUvmIrbuilderNewBb
    | CiUvmIrbuilderNewDestClause
    | CiUvmIrbuilderNewExcClause
    | CiUvmIrbuilderNewKeepaliveClause
    | CiUvmIrbuilderNewCscRetWith
    | CiUvmIrbuilderNewCscKillOld
    | CiUvmIrbuilderNewNscPassValues
    | CiUvmIrbuilderNewNscThrowExc
    | CiUvmIrbuilderNewBinop
    | CiUvmIrbuilderNewBinopWithStatus
    | CiUvmIrbuilderNewCmp
    | CiUvmIrbuilderNewConv
    | CiUvmIrbuilderNewSelect
    | CiUvmIrbuilderNewBranch
    | CiUvmIrbuilderNewBranch2
    | CiUvmIrbuilderNewSwitch
    | CiUvmIrbuilderNewCall
    | CiUvmIrbuilderNewTailcall
    | CiUvmIrbuilderNewRet
    | CiUvmIrbuilderNewThrow
    | CiUvmIrbuilderNewExtractvalue
    | CiUvmIrbuilderNewInsertvalue
    | CiUvmIrbuilderNewExtractelement
    | CiUvmIrbuilderNewInsertelement
    | CiUvmIrbuilderNewShufflevector
    | CiUvmIrbuilderNewNew
    | CiUvmIrbuilderNewNewhybrid
    | CiUvmIrbuilderNewAlloca
    | CiUvmIrbuilderNewAllocahybrid
    | CiUvmIrbuilderNewGetiref
    | CiUvmIrbuilderNewGetfieldiref
    | CiUvmIrbuilderNewGetelemiref
    | CiUvmIrbuilderNewShiftiref
    | CiUvmIrbuilderNewGetvarpartiref
    | CiUvmIrbuilderNewLoad
    | CiUvmIrbuilderNewStore
    | CiUvmIrbuilderNewCmpxchg
    | CiUvmIrbuilderNewAtomicrmw
    | CiUvmIrbuilderNewFence
    | CiUvmIrbuilderNewTrap
    | CiUvmIrbuilderNewWatchpoint
    | CiUvmIrbuilderNewWpbranch
    | CiUvmIrbuilderNewCcall
    | CiUvmIrbuilderNewNewthread
    | CiUvmIrbuilderNewSwapstack
    | CiUvmIrbuilderNewComminst
    | CiUvmExtPrintStats
    | CiUvmExtClearStats
    deriving (Generic)

instance Enum CommInst where
    fromEnum CiUvmNewStack = 0x201
    fromEnum CiUvmKillStack = 0x202
    fromEnum CiUvmThreadExit = 0x203
    fromEnum CiUvmCurrentStack = 0x204
    fromEnum CiUvmSetThreadlocal = 0x205
    fromEnum CiUvmGetThreadlocal = 0x206
    fromEnum CiUvmTr64IsFp = 0x211
    fromEnum CiUvmTr64IsInt = 0x212
    fromEnum CiUvmTr64IsRef = 0x213
    fromEnum CiUvmTr64FromFp = 0x214
    fromEnum CiUvmTr64FromInt = 0x215
    fromEnum CiUvmTr64FromRef = 0x216
    fromEnum CiUvmTr64ToFp = 0x217
    fromEnum CiUvmTr64ToInt = 0x218
    fromEnum CiUvmTr64ToRef = 0x219
    fromEnum CiUvmTr64ToTag = 0x21a
    fromEnum CiUvmFutexWait = 0x220
    fromEnum CiUvmFutexWaitTimeout = 0x221
    fromEnum CiUvmFutexWake = 0x222
    fromEnum CiUvmFutexCmpRequeue = 0x223
    fromEnum CiUvmKillDependency = 0x230
    fromEnum CiUvmNativePin = 0x240
    fromEnum CiUvmNativeUnpin = 0x241
    fromEnum CiUvmNativeGetAddr = 0x242
    fromEnum CiUvmNativeExpose = 0x243
    fromEnum CiUvmNativeUnexpose = 0x244
    fromEnum CiUvmNativeGetCookie = 0x245
    fromEnum CiUvmMetaIdOf = 0x250
    fromEnum CiUvmMetaNameOf = 0x251
    fromEnum CiUvmMetaLoadBundle = 0x252
    fromEnum CiUvmMetaLoadHail = 0x253
    fromEnum CiUvmMetaNewCursor = 0x254
    fromEnum CiUvmMetaNextFrame = 0x255
    fromEnum CiUvmMetaCopyCursor = 0x256
    fromEnum CiUvmMetaCloseCursor = 0x257
    fromEnum CiUvmMetaCurFunc = 0x258
    fromEnum CiUvmMetaCurFuncVer = 0x259
    fromEnum CiUvmMetaCurInst = 0x25a
    fromEnum CiUvmMetaDumpKeepalives = 0x25b
    fromEnum CiUvmMetaPopFramesTo = 0x25c
    fromEnum CiUvmMetaPushFrame = 0x25d
    fromEnum CiUvmMetaEnableWatchpoint = 0x25e
    fromEnum CiUvmMetaDisableWatchpoint = 0x25f
    fromEnum CiUvmMetaSetTrapHandler = 0x260
    fromEnum CiUvmMetaConstantById = 0x268
    fromEnum CiUvmMetaGlobalById = 0x269
    fromEnum CiUvmMetaFuncById = 0x26a
    fromEnum CiUvmMetaExpfuncById = 0x26b
    fromEnum CiUvmIrbuilderNewIrBuilder = 0x270
    fromEnum CiUvmIrbuilderLoad = 0x300
    fromEnum CiUvmIrbuilderAbort = 0x301
    fromEnum CiUvmIrbuilderGenSym = 0x302
    fromEnum CiUvmIrbuilderNewTypeInt = 0x303
    fromEnum CiUvmIrbuilderNewTypeFloat = 0x304
    fromEnum CiUvmIrbuilderNewTypeDouble = 0x305
    fromEnum CiUvmIrbuilderNewTypeUptr = 0x306
    fromEnum CiUvmIrbuilderNewTypeUfuncptr = 0x307
    fromEnum CiUvmIrbuilderNewTypeStruct = 0x308
    fromEnum CiUvmIrbuilderNewTypeHybrid = 0x309
    fromEnum CiUvmIrbuilderNewTypeArray = 0x30a
    fromEnum CiUvmIrbuilderNewTypeVector = 0x30b
    fromEnum CiUvmIrbuilderNewTypeVoid = 0x30c
    fromEnum CiUvmIrbuilderNewTypeRef = 0x30d
    fromEnum CiUvmIrbuilderNewTypeIref = 0x30e
    fromEnum CiUvmIrbuilderNewTypeWeakref = 0x30f
    fromEnum CiUvmIrbuilderNewTypeFuncref = 0x310
    fromEnum CiUvmIrbuilderNewTypeTagref64 = 0x311
    fromEnum CiUvmIrbuilderNewTypeThreadref = 0x312
    fromEnum CiUvmIrbuilderNewTypeStackref = 0x313
    fromEnum CiUvmIrbuilderNewTypeFramecursorref = 0x314
    fromEnum CiUvmIrbuilderNewTypeIrbuilderref = 0x315
    fromEnum CiUvmIrbuilderNewFuncsig = 0x316
    fromEnum CiUvmIrbuilderNewConstInt = 0x317
    fromEnum CiUvmIrbuilderNewConstIntEx = 0x318
    fromEnum CiUvmIrbuilderNewConstFloat = 0x319
    fromEnum CiUvmIrbuilderNewConstDouble = 0x31a
    fromEnum CiUvmIrbuilderNewConstNull = 0x31b
    fromEnum CiUvmIrbuilderNewConstSeq = 0x31c
    fromEnum CiUvmIrbuilderNewConstExtern = 0x31d
    fromEnum CiUvmIrbuilderNewGlobalCell = 0x31e
    fromEnum CiUvmIrbuilderNewFunc = 0x31f
    fromEnum CiUvmIrbuilderNewExpFunc = 0x320
    fromEnum CiUvmIrbuilderNewFuncVer = 0x321
    fromEnum CiUvmIrbuilderNewBb = 0x322
    fromEnum CiUvmIrbuilderNewDestClause = 0x323
    fromEnum CiUvmIrbuilderNewExcClause = 0x324
    fromEnum CiUvmIrbuilderNewKeepaliveClause = 0x325
    fromEnum CiUvmIrbuilderNewCscRetWith = 0x326
    fromEnum CiUvmIrbuilderNewCscKillOld = 0x327
    fromEnum CiUvmIrbuilderNewNscPassValues = 0x328
    fromEnum CiUvmIrbuilderNewNscThrowExc = 0x329
    fromEnum CiUvmIrbuilderNewBinop = 0x32a
    fromEnum CiUvmIrbuilderNewBinopWithStatus = 0x32b
    fromEnum CiUvmIrbuilderNewCmp = 0x32c
    fromEnum CiUvmIrbuilderNewConv = 0x32d
    fromEnum CiUvmIrbuilderNewSelect = 0x32e
    fromEnum CiUvmIrbuilderNewBranch = 0x32f
    fromEnum CiUvmIrbuilderNewBranch2 = 0x330
    fromEnum CiUvmIrbuilderNewSwitch = 0x331
    fromEnum CiUvmIrbuilderNewCall = 0x332
    fromEnum CiUvmIrbuilderNewTailcall = 0x333
    fromEnum CiUvmIrbuilderNewRet = 0x334
    fromEnum CiUvmIrbuilderNewThrow = 0x335
    fromEnum CiUvmIrbuilderNewExtractvalue = 0x336
    fromEnum CiUvmIrbuilderNewInsertvalue = 0x337
    fromEnum CiUvmIrbuilderNewExtractelement = 0x338
    fromEnum CiUvmIrbuilderNewInsertelement = 0x339
    fromEnum CiUvmIrbuilderNewShufflevector = 0x33a
    fromEnum CiUvmIrbuilderNewNew = 0x33b
    fromEnum CiUvmIrbuilderNewNewhybrid = 0x33c
    fromEnum CiUvmIrbuilderNewAlloca = 0x33d
    fromEnum CiUvmIrbuilderNewAllocahybrid = 0x33e
    fromEnum CiUvmIrbuilderNewGetiref = 0x33f
    fromEnum CiUvmIrbuilderNewGetfieldiref = 0x340
    fromEnum CiUvmIrbuilderNewGetelemiref = 0x341
    fromEnum CiUvmIrbuilderNewShiftiref = 0x342
    fromEnum CiUvmIrbuilderNewGetvarpartiref = 0x343
    fromEnum CiUvmIrbuilderNewLoad = 0x344
    fromEnum CiUvmIrbuilderNewStore = 0x345
    fromEnum CiUvmIrbuilderNewCmpxchg = 0x346
    fromEnum CiUvmIrbuilderNewAtomicrmw = 0x347
    fromEnum CiUvmIrbuilderNewFence = 0x348
    fromEnum CiUvmIrbuilderNewTrap = 0x349
    fromEnum CiUvmIrbuilderNewWatchpoint = 0x34a
    fromEnum CiUvmIrbuilderNewWpbranch = 0x34b
    fromEnum CiUvmIrbuilderNewCcall = 0x34c
    fromEnum CiUvmIrbuilderNewNewthread = 0x34d
    fromEnum CiUvmIrbuilderNewSwapstack = 0x34e
    fromEnum CiUvmIrbuilderNewComminst = 0x34f
    fromEnum CiUvmExtPrintStats = 0xc001
    fromEnum CiUvmExtClearStats = 0xc002
    toEnum 0x201 = CiUvmNewStack
    toEnum 0x202 = CiUvmKillStack
    toEnum 0x203 = CiUvmThreadExit
    toEnum 0x204 = CiUvmCurrentStack
    toEnum 0x205 = CiUvmSetThreadlocal
    toEnum 0x206 = CiUvmGetThreadlocal
    toEnum 0x211 = CiUvmTr64IsFp
    toEnum 0x212 = CiUvmTr64IsInt
    toEnum 0x213 = CiUvmTr64IsRef
    toEnum 0x214 = CiUvmTr64FromFp
    toEnum 0x215 = CiUvmTr64FromInt
    toEnum 0x216 = CiUvmTr64FromRef
    toEnum 0x217 = CiUvmTr64ToFp
    toEnum 0x218 = CiUvmTr64ToInt
    toEnum 0x219 = CiUvmTr64ToRef
    toEnum 0x21a = CiUvmTr64ToTag
    toEnum 0x220 = CiUvmFutexWait
    toEnum 0x221 = CiUvmFutexWaitTimeout
    toEnum 0x222 = CiUvmFutexWake
    toEnum 0x223 = CiUvmFutexCmpRequeue
    toEnum 0x230 = CiUvmKillDependency
    toEnum 0x240 = CiUvmNativePin
    toEnum 0x241 = CiUvmNativeUnpin
    toEnum 0x242 = CiUvmNativeGetAddr
    toEnum 0x243 = CiUvmNativeExpose
    toEnum 0x244 = CiUvmNativeUnexpose
    toEnum 0x245 = CiUvmNativeGetCookie
    toEnum 0x250 = CiUvmMetaIdOf
    toEnum 0x251 = CiUvmMetaNameOf
    toEnum 0x252 = CiUvmMetaLoadBundle
    toEnum 0x253 = CiUvmMetaLoadHail
    toEnum 0x254 = CiUvmMetaNewCursor
    toEnum 0x255 = CiUvmMetaNextFrame
    toEnum 0x256 = CiUvmMetaCopyCursor
    toEnum 0x257 = CiUvmMetaCloseCursor
    toEnum 0x258 = CiUvmMetaCurFunc
    toEnum 0x259 = CiUvmMetaCurFuncVer
    toEnum 0x25a = CiUvmMetaCurInst
    toEnum 0x25b = CiUvmMetaDumpKeepalives
    toEnum 0x25c = CiUvmMetaPopFramesTo
    toEnum 0x25d = CiUvmMetaPushFrame
    toEnum 0x25e = CiUvmMetaEnableWatchpoint
    toEnum 0x25f = CiUvmMetaDisableWatchpoint
    toEnum 0x260 = CiUvmMetaSetTrapHandler
    toEnum 0x268 = CiUvmMetaConstantById
    toEnum 0x269 = CiUvmMetaGlobalById
    toEnum 0x26a = CiUvmMetaFuncById
    toEnum 0x26b = CiUvmMetaExpfuncById
    toEnum 0x270 = CiUvmIrbuilderNewIrBuilder
    toEnum 0x300 = CiUvmIrbuilderLoad
    toEnum 0x301 = CiUvmIrbuilderAbort
    toEnum 0x302 = CiUvmIrbuilderGenSym
    toEnum 0x303 = CiUvmIrbuilderNewTypeInt
    toEnum 0x304 = CiUvmIrbuilderNewTypeFloat
    toEnum 0x305 = CiUvmIrbuilderNewTypeDouble
    toEnum 0x306 = CiUvmIrbuilderNewTypeUptr
    toEnum 0x307 = CiUvmIrbuilderNewTypeUfuncptr
    toEnum 0x308 = CiUvmIrbuilderNewTypeStruct
    toEnum 0x309 = CiUvmIrbuilderNewTypeHybrid
    toEnum 0x30a = CiUvmIrbuilderNewTypeArray
    toEnum 0x30b = CiUvmIrbuilderNewTypeVector
    toEnum 0x30c = CiUvmIrbuilderNewTypeVoid
    toEnum 0x30d = CiUvmIrbuilderNewTypeRef
    toEnum 0x30e = CiUvmIrbuilderNewTypeIref
    toEnum 0x30f = CiUvmIrbuilderNewTypeWeakref
    toEnum 0x310 = CiUvmIrbuilderNewTypeFuncref
    toEnum 0x311 = CiUvmIrbuilderNewTypeTagref64
    toEnum 0x312 = CiUvmIrbuilderNewTypeThreadref
    toEnum 0x313 = CiUvmIrbuilderNewTypeStackref
    toEnum 0x314 = CiUvmIrbuilderNewTypeFramecursorref
    toEnum 0x315 = CiUvmIrbuilderNewTypeIrbuilderref
    toEnum 0x316 = CiUvmIrbuilderNewFuncsig
    toEnum 0x317 = CiUvmIrbuilderNewConstInt
    toEnum 0x318 = CiUvmIrbuilderNewConstIntEx
    toEnum 0x319 = CiUvmIrbuilderNewConstFloat
    toEnum 0x31a = CiUvmIrbuilderNewConstDouble
    toEnum 0x31b = CiUvmIrbuilderNewConstNull
    toEnum 0x31c = CiUvmIrbuilderNewConstSeq
    toEnum 0x31d = CiUvmIrbuilderNewConstExtern
    toEnum 0x31e = CiUvmIrbuilderNewGlobalCell
    toEnum 0x31f = CiUvmIrbuilderNewFunc
    toEnum 0x320 = CiUvmIrbuilderNewExpFunc
    toEnum 0x321 = CiUvmIrbuilderNewFuncVer
    toEnum 0x322 = CiUvmIrbuilderNewBb
    toEnum 0x323 = CiUvmIrbuilderNewDestClause
    toEnum 0x324 = CiUvmIrbuilderNewExcClause
    toEnum 0x325 = CiUvmIrbuilderNewKeepaliveClause
    toEnum 0x326 = CiUvmIrbuilderNewCscRetWith
    toEnum 0x327 = CiUvmIrbuilderNewCscKillOld
    toEnum 0x328 = CiUvmIrbuilderNewNscPassValues
    toEnum 0x329 = CiUvmIrbuilderNewNscThrowExc
    toEnum 0x32a = CiUvmIrbuilderNewBinop
    toEnum 0x32b = CiUvmIrbuilderNewBinopWithStatus
    toEnum 0x32c = CiUvmIrbuilderNewCmp
    toEnum 0x32d = CiUvmIrbuilderNewConv
    toEnum 0x32e = CiUvmIrbuilderNewSelect
    toEnum 0x32f = CiUvmIrbuilderNewBranch
    toEnum 0x330 = CiUvmIrbuilderNewBranch2
    toEnum 0x331 = CiUvmIrbuilderNewSwitch
    toEnum 0x332 = CiUvmIrbuilderNewCall
    toEnum 0x333 = CiUvmIrbuilderNewTailcall
    toEnum 0x334 = CiUvmIrbuilderNewRet
    toEnum 0x335 = CiUvmIrbuilderNewThrow
    toEnum 0x336 = CiUvmIrbuilderNewExtractvalue
    toEnum 0x337 = CiUvmIrbuilderNewInsertvalue
    toEnum 0x338 = CiUvmIrbuilderNewExtractelement
    toEnum 0x339 = CiUvmIrbuilderNewInsertelement
    toEnum 0x33a = CiUvmIrbuilderNewShufflevector
    toEnum 0x33b = CiUvmIrbuilderNewNew
    toEnum 0x33c = CiUvmIrbuilderNewNewhybrid
    toEnum 0x33d = CiUvmIrbuilderNewAlloca
    toEnum 0x33e = CiUvmIrbuilderNewAllocahybrid
    toEnum 0x33f = CiUvmIrbuilderNewGetiref
    toEnum 0x340 = CiUvmIrbuilderNewGetfieldiref
    toEnum 0x341 = CiUvmIrbuilderNewGetelemiref
    toEnum 0x342 = CiUvmIrbuilderNewShiftiref
    toEnum 0x343 = CiUvmIrbuilderNewGetvarpartiref
    toEnum 0x344 = CiUvmIrbuilderNewLoad
    toEnum 0x345 = CiUvmIrbuilderNewStore
    toEnum 0x346 = CiUvmIrbuilderNewCmpxchg
    toEnum 0x347 = CiUvmIrbuilderNewAtomicrmw
    toEnum 0x348 = CiUvmIrbuilderNewFence
    toEnum 0x349 = CiUvmIrbuilderNewTrap
    toEnum 0x34a = CiUvmIrbuilderNewWatchpoint
    toEnum 0x34b = CiUvmIrbuilderNewWpbranch
    toEnum 0x34c = CiUvmIrbuilderNewCcall
    toEnum 0x34d = CiUvmIrbuilderNewNewthread
    toEnum 0x34e = CiUvmIrbuilderNewSwapstack
    toEnum 0x34f = CiUvmIrbuilderNewComminst
    toEnum 0xc001 = CiUvmExtPrintStats
    toEnum 0xc002 = CiUvmExtClearStats

instance Binary CommInst

instance Show CommInst where
    show CiUvmNewStack = "@uvm.new_stack"
    show CiUvmKillStack = "@uvm.kill_stack"
    show CiUvmThreadExit = "@uvm.thread_exit"
    show CiUvmCurrentStack = "@uvm.current_stack"
    show CiUvmSetThreadlocal = "@uvm.set_threadlocal"
    show CiUvmGetThreadlocal = "@uvm.get_threadlocal"
    show CiUvmTr64IsFp = "@uvm.tr64.is_fp"
    show CiUvmTr64IsInt = "@uvm.tr64.is_int"
    show CiUvmTr64IsRef = "@uvm.tr64.is_ref"
    show CiUvmTr64FromFp = "@uvm.tr64.from_fp"
    show CiUvmTr64FromInt = "@uvm.tr64.from_int"
    show CiUvmTr64FromRef = "@uvm.tr64.from_ref"
    show CiUvmTr64ToFp = "@uvm.tr64.to_fp"
    show CiUvmTr64ToInt = "@uvm.tr64.to_int"
    show CiUvmTr64ToRef = "@uvm.tr64.to_ref"
    show CiUvmTr64ToTag = "@uvm.tr64.to_tag"
    show CiUvmFutexWait = "@uvm.futex.wait"
    show CiUvmFutexWaitTimeout = "@uvm.futex.wait_timeout"
    show CiUvmFutexWake = "@uvm.futex.wake"
    show CiUvmFutexCmpRequeue = "@uvm.futex.cmp_requeue"
    show CiUvmKillDependency = "@uvm.kill_dependency"
    show CiUvmNativePin = "@uvm.native.pin"
    show CiUvmNativeUnpin = "@uvm.native.unpin"
    show CiUvmNativeGetAddr = "@uvm.native.get_addr"
    show CiUvmNativeExpose = "@uvm.native.expose"
    show CiUvmNativeUnexpose = "@uvm.native.unexpose"
    show CiUvmNativeGetCookie = "@uvm.native.get_cookie"
    show CiUvmMetaIdOf = "@uvm.meta.id_of"
    show CiUvmMetaNameOf = "@uvm.meta.name_of"
    show CiUvmMetaLoadBundle = "@uvm.meta.load_bundle"
    show CiUvmMetaLoadHail = "@uvm.meta.load_hail"
    show CiUvmMetaNewCursor = "@uvm.meta.new_cursor"
    show CiUvmMetaNextFrame = "@uvm.meta.next_frame"
    show CiUvmMetaCopyCursor = "@uvm.meta.copy_cursor"
    show CiUvmMetaCloseCursor = "@uvm.meta.close_cursor"
    show CiUvmMetaCurFunc = "@uvm.meta.cur_func"
    show CiUvmMetaCurFuncVer = "@uvm.meta.cur_func_Ver"
    show CiUvmMetaCurInst = "@uvm.meta.cur_inst"
    show CiUvmMetaDumpKeepalives = "@uvm.meta.dump_keepalives"
    show CiUvmMetaPopFramesTo = "@uvm.meta.pop_frames_to"
    show CiUvmMetaPushFrame = "@uvm.meta.push_frame"
    show CiUvmMetaEnableWatchpoint = "@uvm.meta.enable_watchpoint"
    show CiUvmMetaDisableWatchpoint = "@uvm.meta.disable_watchpoint"
    show CiUvmMetaSetTrapHandler = "@uvm.meta.set_trap_handler"
    show CiUvmMetaConstantById = "@uvm.meta.constant_by_id"
    show CiUvmMetaGlobalById = "@uvm.meta.global_by_id"
    show CiUvmMetaFuncById = "@uvm.meta.func_by_id"
    show CiUvmMetaExpfuncById = "@uvm.meta.expfunc_by_id"
    show CiUvmIrbuilderNewIrBuilder = "@uvm.irbuilder.new_ir_builder"
    show CiUvmIrbuilderLoad = "@uvm.irbuilder.load"
    show CiUvmIrbuilderAbort = "@uvm.irbuilder.abort"
    show CiUvmIrbuilderGenSym = "@uvm.irbuilder.gen_sym"
    show CiUvmIrbuilderNewTypeInt = "@uvm.irbuilder.new_type_int"
    show CiUvmIrbuilderNewTypeFloat = "@uvm.irbuilder.new_type_float"
    show CiUvmIrbuilderNewTypeDouble = "@uvm.irbuilder.new_type_double"
    show CiUvmIrbuilderNewTypeUptr = "@uvm.irbuilder.new_type_uptr"
    show CiUvmIrbuilderNewTypeUfuncptr = "@uvm.irbuilder.new_type_ufuncptr"
    show CiUvmIrbuilderNewTypeStruct = "@uvm.irbuilder.new_type_struct"
    show CiUvmIrbuilderNewTypeHybrid = "@uvm.irbuilder.new_type_hybrid"
    show CiUvmIrbuilderNewTypeArray = "@uvm.irbuilder.new_type_array"
    show CiUvmIrbuilderNewTypeVector = "@uvm.irbuilder.new_type_vector"
    show CiUvmIrbuilderNewTypeVoid = "@uvm.irbuilder.new_type_void"
    show CiUvmIrbuilderNewTypeRef = "@uvm.irbuilder.new_type_ref"
    show CiUvmIrbuilderNewTypeIref = "@uvm.irbuilder.new_type_iref"
    show CiUvmIrbuilderNewTypeWeakref = "@uvm.irbuilder.new_type_weakref"
    show CiUvmIrbuilderNewTypeFuncref = "@uvm.irbuilder.new_type_funcref"
    show CiUvmIrbuilderNewTypeTagref64 = "@uvm.irbuilder.new_type_tagref64"
    show CiUvmIrbuilderNewTypeThreadref = "@uvm.irbuilder.new_type_threadref"
    show CiUvmIrbuilderNewTypeStackref = "@uvm.irbuilder.new_type_stackref"
    show CiUvmIrbuilderNewTypeFramecursorref = "@uvm.irbuilder.new_type_framecursorref"
    show CiUvmIrbuilderNewTypeIrbuilderref = "@uvm.irbuilder.new_type_irbuilderref"
    show CiUvmIrbuilderNewFuncsig = "@uvm.irbuilder.new_funcsig"
    show CiUvmIrbuilderNewConstInt = "@uvm.irbuilder.new_const_int"
    show CiUvmIrbuilderNewConstIntEx = "@uvm.irbuilder.new_const_int_ex"
    show CiUvmIrbuilderNewConstFloat = "@uvm.irbuilder.new_const_float"
    show CiUvmIrbuilderNewConstDouble = "@uvm.irbuilder.new_const_double"
    show CiUvmIrbuilderNewConstNull = "@uvm.irbuilder.new_const_null"
    show CiUvmIrbuilderNewConstSeq = "@uvm.irbuilder.new_const_seq"
    show CiUvmIrbuilderNewConstExtern = "@uvm.irbuilder.new_const_extern"
    show CiUvmIrbuilderNewGlobalCell = "@uvm.irbuilder.new_global_cell"
    show CiUvmIrbuilderNewFunc = "@uvm.irbuilder.new_func"
    show CiUvmIrbuilderNewExpFunc = "@uvm.irbuilder.new_exp_func"
    show CiUvmIrbuilderNewFuncVer = "@uvm.irbuilder.new_func_ver"
    show CiUvmIrbuilderNewBb = "@uvm.irbuilder.new_bb"
    show CiUvmIrbuilderNewDestClause = "@uvm.irbuilder.new_dest_clause"
    show CiUvmIrbuilderNewExcClause = "@uvm.irbuilder.new_exc_clause"
    show CiUvmIrbuilderNewKeepaliveClause = "@uvm.irbuilder.new_keepalive_clause"
    show CiUvmIrbuilderNewCscRetWith = "@uvm.irbuilder.new_csc_ret_with"
    show CiUvmIrbuilderNewCscKillOld = "@uvm.irbuilder.new_csc_kill_old"
    show CiUvmIrbuilderNewNscPassValues = "@uvm.irbuilder.new_nsc_pass_values"
    show CiUvmIrbuilderNewNscThrowExc = "@uvm.irbuilder.new_nsc_throw_exc"
    show CiUvmIrbuilderNewBinop = "@uvm.irbuilder.new_binop"
    show CiUvmIrbuilderNewBinopWithStatus = "@uvm.irbuilder.new_binop_with_status"
    show CiUvmIrbuilderNewCmp = "@uvm.irbuilder.new_cmp"
    show CiUvmIrbuilderNewConv = "@uvm.irbuilder.new_conv"
    show CiUvmIrbuilderNewSelect = "@uvm.irbuilder.new_select"
    show CiUvmIrbuilderNewBranch = "@uvm.irbuilder.new_branch"
    show CiUvmIrbuilderNewBranch2 = "@uvm.irbuilder.new_branch2"
    show CiUvmIrbuilderNewSwitch = "@uvm.irbuilder.new_switch"
    show CiUvmIrbuilderNewCall = "@uvm.irbuilder.new_call"
    show CiUvmIrbuilderNewTailcall = "@uvm.irbuilder.new_tailcall"
    show CiUvmIrbuilderNewRet = "@uvm.irbuilder.new_ret"
    show CiUvmIrbuilderNewThrow = "@uvm.irbuilder.new_throw"
    show CiUvmIrbuilderNewExtractvalue = "@uvm.irbuilder.new_extractvalue"
    show CiUvmIrbuilderNewInsertvalue = "@uvm.irbuilder.new_insertvalue"
    show CiUvmIrbuilderNewExtractelement = "@uvm.irbuilder.new_extractelement"
    show CiUvmIrbuilderNewInsertelement = "@uvm.irbuilder.new_insertelement"
    show CiUvmIrbuilderNewShufflevector = "@uvm.irbuilder.new_shufflevector"
    show CiUvmIrbuilderNewNew = "@uvm.irbuilder.new_new"
    show CiUvmIrbuilderNewNewhybrid = "@uvm.irbuilder.new_newhybrid"
    show CiUvmIrbuilderNewAlloca = "@uvm.irbuilder.new_alloca"
    show CiUvmIrbuilderNewAllocahybrid = "@uvm.irbuilder.new_allocahybrid"
    show CiUvmIrbuilderNewGetiref = "@uvm.irbuilder.new_getiref"
    show CiUvmIrbuilderNewGetfieldiref = "@uvm.irbuilder.new_getfieldiref"
    show CiUvmIrbuilderNewGetelemiref = "@uvm.irbuilder.new_getelemiref"
    show CiUvmIrbuilderNewShiftiref = "@uvm.irbuilder.new_shiftiref"
    show CiUvmIrbuilderNewGetvarpartiref = "@uvm.irbuilder.new_getvarpartiref"
    show CiUvmIrbuilderNewLoad = "@uvm.irbuilder.new_load"
    show CiUvmIrbuilderNewStore = "@uvm.irbuilder.new_store"
    show CiUvmIrbuilderNewCmpxchg = "@uvm.irbuilder.new_cmpxchg"
    show CiUvmIrbuilderNewAtomicrmw = "@uvm.irbuilder.new_atomicrmw"
    show CiUvmIrbuilderNewFence = "@uvm.irbuilder.new_fence"
    show CiUvmIrbuilderNewTrap = "@uvm.irbuilder.new_trap"
    show CiUvmIrbuilderNewWatchpoint = "@uvm.irbuilder.new_watchpoint"
    show CiUvmIrbuilderNewWpbranch = "@uvm.irbuilder.new_wpbranch"
    show CiUvmIrbuilderNewCcall = "@uvm.irbuilder.new_ccall"
    show CiUvmIrbuilderNewNewthread = "@uvm.irbuilder.new_newthread"
    show CiUvmIrbuilderNewSwapstack = "@uvm.irbuilder.new_swapstack"
    show CiUvmIrbuilderNewComminst = "@uvm.irbuilder.new_comminst"
    show CiUvmExtPrintStats = "@uvm.ext.print_stats"
    show CiUvmExtClearStats = "@uvm.ext.clear_stats"
