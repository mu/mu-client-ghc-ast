# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple
from typing import Dict

import sys

import pprint
import muapiparser

def print_indent(value, **kwargs):
    print('    ' + value, **kwargs)

def slurp(fn):
    """Read the contents of a file into memory and dump the string.
    """
    with open(fn) as f:
        return f.read()


def pascalToCamel(string, startLowercase=True):
    """Turn a Pascal_Cased string into a CamelCase string with optional
    lowercased first letter.
    """
    if not string:
        return ""

    result = ""

    for word in string.lower().split('_'):
        if word:
            result += word[0].upper() + word[1:]

    if startLowercase:
        result = result[0].lower() + result[1:]

    return result

def get_comminsts(stuff):
    index = -1
    for i in range(len(stuff["enums"])):
        if stuff["enums"][i]["name"] == "MuCommInst":
            index = i
            break
            
    return stuff["enums"][index]["defs"]    
    
def print_def(insts):
    print("data CommInst")
    print_indent("= {}".format(pascalToCamel(insts[0]["name"])[2:]))
    for i in range(1, len(insts)):
        print_indent("| {}".format(pascalToCamel(insts[i]["name"])[2:]))
    print_indent("deriving (Generic)")
    
def print_show(insts):
    print ("instance Show CommInst where")
    for inst in insts:
        print_indent("show {} = \"{}\"".format(pascalToCamel(inst["name"])[2:], inst["muname"]))
    
def print_enum(insts):
    print("instance Enum CommInst where")
    for inst in insts:
        print_indent("fromEnum {} = {}".format(pascalToCamel(inst["name"])[2:], inst["value"]))
    for inst in insts:
        print_indent("toEnum {} = {}".format(inst["value"], pascalToCamel(inst["name"])[2:]))

def print_module_header():
    print("""--
-- Copyright 2017 The Australian National University
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

{-# LANGUAGE DeriveGeneric #-}

module Mu.AST.CommInst (CommInst (..)) where

import Data.Binary (Binary)
import GHC.Generics (Generic)
""")

def print_haskell_file(stuff):
    print_module_header()
    insts = get_comminsts(stuff)
    print_def(insts)
    print("")
    print_enum(insts)
    print("")
    print("instance Binary CommInst")
    print("")
    print_show(insts)

if __name__ == '__main__':
    stuff = muapiparser.parse_muapi(slurp(sys.argv[1]))
    print_haskell_file(stuff)
